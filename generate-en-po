#!/usr/bin/env python
# encoding: utf-8

# Python 2/3 compatibility
from __future__ import division, absolute_import, print_function, unicode_literals

import argparse
import os
import shutil
import subprocess
import sys
import tempfile

import xml.etree.ElementTree as ET

SINGULAR_FIELD = "singular"
PLURAL_FIELD = "plural"

DESCRIPTION = """\
Generate a plural-only English .po file for Qt code using the Qt translation
system.
"""

EPILOG = """\
Strings in code must be annotated like this:

    //~ {SINGULAR_FIELD} %n file
    //~ {PLURAL_FIELD} %n files
    str = QObject::tr("%n file(s)", "", i);
""".format(SINGULAR_FIELD=SINGULAR_FIELD, PLURAL_FIELD=PLURAL_FIELD)


def create_error_message(message_elt, field):
    location_elt = message_elt.find("location")
    filename = location_elt.get("filename")
    line = location_elt.get("line")
    source = message_elt.find("source").text
    return "{filename}:{line}: '{source}' message is missing '//~ {field}' comment".format(**locals())


def process_messages(message_elt_lst):
    errors = []
    for message_elt in message_elt_lst:
        translation_elt = message_elt.find("translation")
        translation_elt.clear()
        missing_fields = []
        for field in SINGULAR_FIELD, PLURAL_FIELD:
            extra_elt = message_elt.find("extra-" + field)
            if extra_elt is None:
                errors.append(create_error_message(message_elt, field))
                continue
            numerusform_elt = ET.SubElement(translation_elt, "numerusform")
            numerusform_elt.text = extra_elt.text
    return errors


def main():
    parser = argparse.ArgumentParser(
            description=DESCRIPTION,
            epilog=EPILOG,
            formatter_class=argparse.RawDescriptionHelpFormatter
            )

    parser.add_argument("-o", "--output", required=True,
        help="Write po file to FILE", metavar="FILE")

    parser.add_argument("-I", "--include_path", action='append',
        help="Add include PATH", metavar="PATH")

    parser.add_argument("src_files", nargs="+",
        help="Source files to parse")

    args = parser.parse_args()
    
    include_paths = []
    if args.include_path is not None:
        include_paths = ["-I" + i for i in args.include_path]

    lupdate = os.environ.get("LUPDATE", "lupdate")
    lconvert = os.environ.get("LCONVERT", "lconvert")

    # Create tmpdir in current dir so that filenames in error output do not
    # contain the absolute path to the source files
    tmpdir = tempfile.mkdtemp(prefix="tmp-generate-en-po-", dir=os.getcwd())
    try:
        tmpfile_in = os.path.join(tmpdir, "tmp-in.ts")
        tmpfile_out = os.path.join(tmpdir, "tmp-out.ts")
        subprocess.check_call(
                ["env", lupdate, "-silent", "-pluralonly"] + include_paths + args.src_files + ["-ts", tmpfile_in]
                )

        tree = ET.parse(tmpfile_in)
        messages = list(tree.iter("message"))
        if not messages:
            # No plural messages. Do not create a .po, rm it if it was there
            if os.path.exists(args.output):
                os.unlink(args.output)
            return 0
        errors = process_messages(messages)
        if errors:
            for error in errors:
                print(error, file=sys.stderr)
            return 1
        tree.write(tmpfile_out)

        subprocess.check_call(
                ["env", lconvert, tmpfile_out, "-sort-contexts", "-output-format", "po", "-target-language", "en", "-o", args.output]
                )
    finally:
        shutil.rmtree(tmpdir)

    return 0


if __name__ == "__main__":
    sys.exit(main())
# vi: ts=4 sw=4 et
